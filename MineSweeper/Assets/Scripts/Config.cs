﻿using UnityEngine;

public class Config : MonoBehaviour
{
    public Material closed;
    public Material revealed;
    public Material bomb;
    public Material marked;
    public GameObject prefab;

    public int Bombs { get; set; }
    public int Tiles { get; set; }

    void Awake()
    {
        Tiles = PlayerPrefs.GetInt("Diff");
        Bombs = PlayerPrefs.GetInt("Bombs");
    }
}
