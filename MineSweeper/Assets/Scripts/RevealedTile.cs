﻿public class RevealedTile : TileState
{
    public RevealedTile()
    {
        activeMaterial = ConfigSingleton.Config.revealed;
    }
    public override TileState Mark()
    {
        return this;
    }
    public override TileState Reveal()
    {
        return this;
    }
}
