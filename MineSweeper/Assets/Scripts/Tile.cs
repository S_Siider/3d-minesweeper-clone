﻿using UnityEngine;
using System;

//An object that holds all the information about a particular tile on the field
public class Tile
{
    Material material;
    GameObject physicalTile;
    TileState state; //State of the tile
    TextMesh textMesh; //The number of the tile

    //Physical coordinates of the tile
    float rx;
    float rz;

    //Ternary value, that is used when recursivley revealing tiles
    //0 = Shouldn't check current, 1 = Check current but don't go further, 2 = Check current and surroundings
    int shouldCheck;

    //How many bombs there are surrounding
    int surroundingBombs;

    //Event for giving info the GameField
    public event EventHandler<OnNotifyFromTileArgs> NotifyEvent;

    public Tile(int x, int z)
    {
        //Write down what the prefab coordinates of this object will always be (Hardcoded ugliness)
        /* x = grid coordinaate
         * 60f is the size of the playing field (whole room is about 61f, so 0.5f on each side as extra padding)
         * 30f is added to align the playing field to the center (so the padding would be perfect all around)
         * Then we divide by how many tiles are we allowed to have
         * And the subtract -30 as our playing field does ot start from 0-coordinates
         */
        int tilesInARow = ConfigSingleton.Config.Tiles;
        rx = (x * 60f + 30f) / tilesInARow - 30f;
        rz = (z * 60f + 30f) / tilesInARow - 30f;

        //Value the prefab wrapper object we get from the pool, the reference to the prefab currently it uses and the state of the tile
        state = new ClosedTile();
        state.NotifyEvent += OnStateNotify; //Subscribe to the state's event
        shouldCheck = 2; //All closed tiles are checkable

        physicalTile = UnityEngine.Object.Instantiate(ConfigSingleton.Config.prefab);
        material = ConfigSingleton.Config.closed;
        physicalTile.GetComponent<Renderer>().material = material;
        /* Change the scale of the tile
         * 58f is chosen to allow a total of 2f padding between tiles, which is divided amongst them
         * Using the config to see how many tiles are needed and then calculating how big each tile can be
         * Divide by two because our smallest prefab was for a 2x1x2 piece.
        */
        float scale = 58.0f / ConfigSingleton.Config.Tiles / 2;
        physicalTile.transform.localScale = new Vector3(scale, 0.001f, scale);
        physicalTile.transform.parent = GameObject.Find("Tiles").transform;
    }

    //Return true if the look of the tile needs to be changed
    public bool Mark()
    {
        state = state.Mark();
        state.NotifyEvent += OnStateNotify;
        return CheckMaterial();
    }

    //Return true if the look of the tile needs to be changed
    public bool Reveal()
    {
        shouldCheck = 0; //Revealed tiles dont need to be checked
        state = state.Reveal();
        state.NotifyEvent += OnStateNotify;
        return CheckMaterial();
    }

    private bool CheckMaterial()
    {
        if (!material.Equals(state.GetActiveMaterial()))
        {
            material = state.GetActiveMaterial();
            physicalTile.GetComponent<Renderer>().material = material;
            return true;
        }
        return false;
    }

    public void AddBomb()
    {
        surroundingBombs++;
        shouldCheck = 1; //Tiles with surrounding bombs are meant to be checked only once
    }

    public float GetRealX()
    {
        return rx;
    }

    public float GetRealZ()
    {
        return rz;
    }

    public void SetPosition(Vector3 pos)
    {
        physicalTile.transform.position = pos;
    }

    public Vector3 GetPosition()
    {
        return physicalTile.transform.position;
    }

    public void SetBomb()
    {
        state = new BombTile();
        state.NotifyEvent += OnStateNotify; ;

        shouldCheck = 0; //Bombs are not to be checked
    }

    public void SetTextMesh(TextMesh textMesh)
    {
        this.textMesh = textMesh;
    }

    public int ShouldCheck()
    {
        return shouldCheck;
    }

    //Observer method that gets a notice if the state of the tilestate changes, and then notifies the GameField
    protected void OnStateNotify(object sender, TileState.OnNotifyFromStateArgs a)
    {
        if (a.State is RevealedTile)
        {
            OnNotifyEvent(new OnNotifyFromTileArgs("Reveal"));
            if (surroundingBombs != 0)
            {
                textMesh.text = surroundingBombs.ToString();
            }
        }
        else if (a.State is RevealedBombTile)
        {
            OnNotifyEvent(new OnNotifyFromTileArgs("Bomb"));
        }
        else if (a.State is MarkedTile || a.State is MarkedBombTile)
        {
            OnNotifyEvent(new OnNotifyFromTileArgs("Mark"));
        }
        else if (a.State is ClosedTile || a.State is BombTile)
        {
            OnNotifyEvent(new OnNotifyFromTileArgs("Unmark"));
        }
    }

    protected void OnNotifyEvent(OnNotifyFromTileArgs a)
    {
        if (NotifyEvent != null)
        {
            NotifyEvent(this, a);
        }
    }

    public class OnNotifyFromTileArgs : EventArgs
    {
        public OnNotifyFromTileArgs(String s)
        {
            Message = s;
        }
        public String Message { get; set; }
    }
}
