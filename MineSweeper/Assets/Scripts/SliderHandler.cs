﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SliderHandler : MonoBehaviour
{

    public void Start()
    {
        if (PlayerPrefs.GetInt("Diff") == 0 || PlayerPrefs.GetInt("Bombs") == 0)
        {
            PlayerPrefs.SetInt("Diff", 20);
            PlayerPrefs.SetInt("Bombs", 50);
            PlayerPrefs.SetFloat("BombSlider", 0.0f);
            PlayerPrefs.SetFloat("TileSlider", 0.18f);
        }
        else
        {
            GameObject.Find("Bombs").GetComponent<Slider>().value = PlayerPrefs.GetFloat("BombSlider");
            GameObject.Find("Tiles").GetComponent<Slider>().value = PlayerPrefs.GetFloat("TileSlider");
        }
        Debug.Log("Difficulty: " + PlayerPrefs.GetInt("Diff") + ", Bombs: " + PlayerPrefs.GetInt("Bombs"));
        GameObject.Find("TileText").GetComponent<TextMeshProUGUI>().text = "TILES: " + PlayerPrefs.GetInt("Diff") + " X " + PlayerPrefs.GetInt("Diff");
        GameObject.Find("BombText").GetComponent<TextMeshProUGUI>().text = "BOMBS: " + PlayerPrefs.GetInt("Bombs");

    }

    public void SetDifficulty()
    {
        PlayerPrefs.SetInt("Diff", (int)(47.0f * GameObject.Find("Tiles").GetComponent<Slider>().value + 3.0f));
        int tiles = PlayerPrefs.GetInt("Diff");
        PlayerPrefs.SetInt("Bombs", (int)((tiles * tiles / 8.0f) + ((tiles * tiles / 8.0f) * GameObject.Find("Bombs").GetComponent<Slider>().value)));
        Debug.Log("Difficulty: " + PlayerPrefs.GetInt("Diff") + ", Bombs: " + PlayerPrefs.GetInt("Bombs"));
        GameObject.Find("TileText").GetComponent<TextMeshProUGUI>().text = "TILES: " + PlayerPrefs.GetInt("Diff") + " X " + PlayerPrefs.GetInt("Diff");
        GameObject.Find("BombText").GetComponent<TextMeshProUGUI>().text = "BOMBS: " + PlayerPrefs.GetInt("Bombs");
        PlayerPrefs.SetFloat("TileSlider", GameObject.Find("Tiles").GetComponent<Slider>().value);
    }

    public void SetBombs()
    {
        int tiles = PlayerPrefs.GetInt("Diff");
        PlayerPrefs.SetInt("Bombs", (int)((tiles * tiles / 8.0f) + ((tiles * tiles / 8.0f) * GameObject.Find("Bombs").GetComponent<Slider>().value)));
        Debug.Log("Difficulty: " + PlayerPrefs.GetInt("Diff") + ", Bombs: " + PlayerPrefs.GetInt("Bombs"));
        GameObject.Find("BombText").GetComponent<TextMeshProUGUI>().text = "BOMBS: " + PlayerPrefs.GetInt("Bombs");
        PlayerPrefs.SetFloat("BombSlider", GameObject.Find("Bombs").GetComponent<Slider>().value);
    }
}
