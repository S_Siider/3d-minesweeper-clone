﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class InGameMenuHandler : MonoBehaviour {

    public void Start()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        Invoke("SlowDown", 0.3f);
    }
	
	void Update () {
		if (Input.GetKey(KeyCode.Escape))
        {
            Time.timeScale = 1.0f;
            Invoke("HandleEsc", 0.3f);
        }
	}

    public void SlowDown()
    {
        Time.timeScale = 0.0f;
    }

    public void ResumeGame()
    {
        SceneManager.UnloadSceneAsync("InGameMenuScene");
        Time.timeScale = 1.0f;
    }

    public void NewGame()
    {
        SceneManager.UnloadSceneAsync("Scene");
        SceneManager.LoadScene("MenuScene");
        Time.timeScale = 1.0f;
    }

    public void HandleEsc()
    {
        if (SceneManager.GetSceneByName("InGameMenuScene").isLoaded)
            ResumeGame();
    }

    public void QuitGame()
    {
        Application.Quit();
    }

}
