﻿using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameOverScript : MonoBehaviour {

    public void Start()
    {
        GameObject.Find("ResultText").GetComponent<TextMeshProUGUI>().text = PlayerPrefs.GetString("Result");
        GameObject.Find("TimeText").GetComponent<TextMeshProUGUI>().text = PlayerPrefs.GetString("Time");
        GameObject.Find("TilesOpenedText").GetComponent<TextMeshProUGUI>().text = PlayerPrefs.GetString("Revealed");
        GameObject.Find("DifficultyText").GetComponent<TextMeshProUGUI>().text = PlayerPrefs.GetString("Difficulty");
    }

    public void NewGame () {
        SceneManager.LoadScene("MenuScene");
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
