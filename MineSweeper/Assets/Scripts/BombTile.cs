﻿public class BombTile : TileState
{
    public BombTile()
    {
        activeMaterial = ConfigSingleton.Config.closed;
    }
    public override TileState Mark()
    {
        TileState state = new MarkedBombTile();
        OnNotifyEvent(new OnNotifyFromStateArgs(state));
        return state;
    }
    public override TileState Reveal()
    {
        TileState state = new RevealedBombTile();
        OnNotifyEvent(new OnNotifyFromStateArgs(state));
        return state;
    }
}
