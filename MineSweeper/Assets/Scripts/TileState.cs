﻿using System;
using UnityEngine;

public class TileState
{
    protected Material activeMaterial;

    public Material GetActiveMaterial()
    {
        return activeMaterial;
    }

    public virtual TileState Mark() { return this; }
    public virtual TileState Reveal() { return this; }
    public event EventHandler<OnNotifyFromStateArgs> NotifyEvent;

    protected virtual void OnNotifyEvent(OnNotifyFromStateArgs a)
    {
        if (NotifyEvent != null)
        {
            NotifyEvent(this, a);
        }
    }

    public class OnNotifyFromStateArgs : EventArgs
    {
        public OnNotifyFromStateArgs(TileState s)
        {
            State = s;
        }
        public TileState State { get; set; }
    }
}
