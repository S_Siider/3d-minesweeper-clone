﻿using UnityEngine;

public class ConfigSingleton : MonoBehaviour
{
    private static Config config;

    public static Config Config
    {
        get
        {
            if (config == null)
            {
                config = FindObjectOfType<Config>();
            }
            return config;
        }
    }
}
