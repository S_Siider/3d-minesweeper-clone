﻿public class ClosedTile : TileState
{
    public ClosedTile()
    {
        activeMaterial = ConfigSingleton.Config.closed;
    }
    public override TileState Mark()
    {
        TileState state = new MarkedTile();
        OnNotifyEvent(new OnNotifyFromStateArgs(state));
        return state;
    }
    public override TileState Reveal()
    {
        TileState state = new RevealedTile();
        OnNotifyEvent(new OnNotifyFromStateArgs(state));
        return state;
    }
}
