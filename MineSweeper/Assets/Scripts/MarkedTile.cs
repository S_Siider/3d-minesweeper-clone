﻿public class MarkedTile : TileState
{
    public MarkedTile()
    {
        activeMaterial = ConfigSingleton.Config.marked;
    }
    public override TileState Mark()
    {
        TileState state = new ClosedTile();
        OnNotifyEvent(new OnNotifyFromStateArgs(state));
        return state;
    }
    public override TileState Reveal()
    {
        return this;
    }
}
