using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class InputHandler : MonoBehaviour
{
    float rotationY = 0.0f;
    float rotationX = 0.0f;
    GameField field;

    void Start()
    {
        field = FindObjectOfType<GameField>();
        Vector3 rotation = transform.localRotation.eulerAngles;  //rotation -> eulerangles
        rotationY = rotation.y;
        rotationX = rotation.x;
    }

    // Has the same functionality as Update(), but recommended to use this when using RigidBodys
    void FixedUpdate () {
        if (SceneManager.sceneCount == 1 && SceneManager.GetSceneByName("Scene").isLoaded)
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
            HandleMouse();
            HandleKeyboard();
            UpdateTime();
        }
    }
    
    void UpdateTime()
    {
        GameObject.Find("TimeText").GetComponent<Text>().text = "Time: " + Time.timeSinceLevelLoad;
    }
    
    private void HandleMouse()
    {
        if (Input.GetMouseButtonDown(0)) HandleTile(true); //Reveal if left-click
        else if (Input.GetMouseButtonDown(1)) HandleTile(false);

        //Rotate the camera according to the mouse movement
        rotationY += Input.GetAxis("Mouse X") * 100.0f * Time.deltaTime;
        rotationX += -Input.GetAxis("Mouse Y") * 100.0f * Time.deltaTime;

        transform.rotation = Quaternion.Euler(transform.rotation.x, rotationY, 0.0f);

        //Looking up and looking down checks
        if (rotationX > 70.0f) rotationX = 70.0f;
        else if (rotationX < -50.0f) rotationX = -50.0f;

        Quaternion camRotation = Quaternion.Euler(rotationX, rotationY, 0.0f);
        Camera.main.transform.rotation = camRotation;
    }

    //Deal with raycasting and finding the tile that was clicked on.
    private void HandleTile(bool reveal)
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        //Distance is limited to 10 units, playing field is 60x60 units, only catch hits on "Tile" objects
        if (Physics.Raycast(ray, out hit) && hit.distance < 10 && hit.transform.name.Contains("Tile"))
        {
            //Find the nearest tile and call a method on it based on which mousebutton was clicked
            Tile nearestTile = field.FindNearestTile(hit.transform.position);
            if (nearestTile != null)
            {
                if (reveal)
                {
                    field.Reveal(nearestTile);
                }
                else
                {
                    field.Mark(nearestTile);
                }
            }
        }
    }

    //Keyboard input
    private void HandleKeyboard()
    {
        Vector3 v = Vector3.zero;
        if (Input.GetKey(KeyCode.Escape))
        {
            Invoke("LoadMenu", 0.3f);
        }
        if (Input.GetKey(KeyCode.W))
            v += transform.forward;
        if (Input.GetKey(KeyCode.S))
            v -= transform.forward;
        if (Input.GetKey(KeyCode.A))
            v -= transform.right;
        if (Input.GetKey(KeyCode.D))
            v += transform.right;
        if (v != Vector3.zero)
            // If movemenet has happened and the cube is not near a wall, then LShift can be used to determine the speed of the player
            if (Input.GetKey(KeyCode.LeftShift) && CubeNotNearWall())
            {
                Walk(v * 12.0f * Time.deltaTime);
            }
            else
            {
                Walk(v * 4.0f * Time.deltaTime);
            }
    }

    private void LoadMenu()
    {
        if (!SceneManager.GetSceneByName("IngameMenuScene").isLoaded)
            SceneManager.LoadScene("InGameMenuScene", LoadSceneMode.Additive);
    }


    private void Walk(Vector3 pos)
    {
        transform.position += pos;
    }

    private bool CubeNotNearWall()
    {
        float x = transform.position.x;
        float z = transform.position.z;
        return z >= -30f && x >= -30f && z <= 30f && x <= 30f;
    }
}
