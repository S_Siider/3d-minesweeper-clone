﻿public class RevealedBombTile : TileState
{
    public RevealedBombTile()
    {
        activeMaterial = ConfigSingleton.Config.bomb;
    }
    public override TileState Mark()
    {
        return this;
    }
    public override TileState Reveal()
    {
        return this;
    }
}
