﻿public class MarkedBombTile : TileState
{
    public MarkedBombTile()
    {
        activeMaterial = ConfigSingleton.Config.marked;
    }
    public override TileState Mark()
    {
        TileState state = new BombTile();
        OnNotifyEvent(new OnNotifyFromStateArgs(state));
        return state;
    }
    public override TileState Reveal()
    {
        return this;
    }
}

