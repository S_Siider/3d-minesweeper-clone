﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameField : MonoBehaviour
{
    List<Tile> tiles;
    int totalBombs;
    int tilesInARow;
    int totalTiles;

    //For statistics
    int markedTiles;
    int openTiles;

    void Start()
    {
        totalBombs = ConfigSingleton.Config.Bombs;
        tilesInARow = ConfigSingleton.Config.Tiles;
        totalTiles = tilesInARow * tilesInARow;
        markedTiles = 0;
        openTiles = 0;
        
        tiles = new List<Tile>(totalTiles);

        //A holder object for all the numbers
        GameObject numbers = new GameObject { name = "Numbers" };

        //Initialize the playing field
        for (int x = 0; x < tilesInARow; x++)
        {
            for (int z = 0; z < tilesInARow; z++)
            {
                //Create the tile object.
                Tile tile = new Tile(x, z);
                tile.NotifyEvent += OnTileNotify; //Subscribe to the NotifyEvent of a tile

                //Move the prefab to the correct position
                Vector3 pos = new Vector3(tile.GetRealX(), 0.001f, tile.GetRealZ());
                tile.SetPosition(pos);
                tiles.Add(tile);

                //Create a GameObject for the number
                GameObject go = new GameObject { name = "Number" };
                go.transform.parent = numbers.transform; //Group the number under numbers gameobject
                go.transform.rotation = Quaternion.Euler(new Vector3(90.0f, 0.0f, 0.0f));

                //More hardcoded ugly to make the text resize according to how many tiles there are
                //Here we calculate the distance between two grid point centers, divide them to find the distance to edge from the center of a tile and then take 80% of that to give some padding to the text
                float distance = (((x * 60f + 30f) / tilesInARow - 30f) - (((x + 1) * 60f + 30f) / tilesInARow - 30f)) * 2 / 5;
                float scale = 58.0f / tilesInARow;
                go.transform.position = new Vector3(pos.x + distance / 2, pos.y, pos.z - distance);
                go.transform.localScale = new Vector3(scale, scale, 1f);

                TextMesh mesh = go.AddComponent<TextMesh>();
                mesh.color = Color.white;
                Resize(mesh);
                tile.SetTextMesh(mesh);
            }
        }

        //Make bombs
        List<Tile> _tiles = new List<Tile>(tiles);
        for (int i = 0; i < totalBombs && _tiles.Count > 0; i++)
        {
            int nr = UnityEngine.Random.Range(0, _tiles.Count);
            Tile tile = _tiles[nr];
            _tiles.RemoveAt(nr);
            tile.SetBomb();

            //Look at all the surrounding colliders and add bombs to all the Tiles around it.
            Collider[] colliders = Physics.OverlapSphere(tile.GetPosition(), 1.5f * (60f / tilesInARow));
            foreach (Collider collider in colliders)
            {
                if (collider.gameObject.transform.name.Contains("Tile"))
                {
                    FindNearestTile(collider.transform.position).AddBomb();

                }
            }
        }
        GameObject.Find("BombsText").GetComponent<Text>().text = "Bombs: " + totalBombs;
        GameObject.Find("TileText").GetComponent<Text>().text = "Closed: " + totalTiles;
        GameObject.Find("MarkedText").GetComponent<Text>().text = "Marked: " + markedTiles;
    }

    private void Resize(TextMesh textMesh)
    {
        //Sharpens the image https://answers.unity.com/questions/20633/textmesh-looking-fuzzy.html
        float ph = Camera.main.pixelHeight;
        float ch = Camera.main.orthographicSize;
        float pixelRatio = (ch * 2.0f) / ph;
        float targetRes = 8f;
        textMesh.characterSize = pixelRatio * Camera.main.orthographicSize / Math.Max(transform.localScale.x, transform.localScale.y);
        textMesh.fontSize = (int)Math.Round(targetRes / textMesh.characterSize);
    }

    public void Reveal(Tile tile)
    {
        int checkValue = tile.ShouldCheck(); //Keep the original checkValue in memory
        if (tile.Reveal())
        {
            if (checkValue == 2) //Only if its a tile with no surrounding bombs do we go to tell others to reveal themselves
            {
                Collider[] colliders = Physics.OverlapSphere(tile.GetPosition(), 1.5f * (60f / tilesInARow));
                foreach (Collider collider in colliders)
                {
                    if (collider.gameObject.transform.name.Contains("Tile"))
                    {
                        Tile _tile = FindNearestTile(collider.transform.position);
                        if (_tile.ShouldCheck() != 0) //Check if we should check the tile at all. 0 == NoCheck
                        {
                            Reveal(_tile);
                        }
                    }
                }
            }
        }
    }

    public void Mark(Tile tile)
    {
        tile.Mark();
    }

    private void IncreaseMarkedTiles()
    {
        markedTiles++;
        GameObject.Find("MarkedText").GetComponent<Text>().text = "Marked: " + markedTiles;
    }

    private void DecreaseMarkedTiles()
    {
        markedTiles--;
        GameObject.Find("MarkedText").GetComponent<Text>().text = "Marked: " + markedTiles;
    }

    private void IncreaseRevealedTiles()
    {
        openTiles++;
        GameObject.Find("TileText").GetComponent<Text>().text = "Closed: " + (totalTiles - openTiles);
    }

    public List<Tile> GetTiles()
    {
        return tiles;
    }

    public Tile FindNearestTile(Vector3 pos)
    {
        /* This works, but goes through the whole list of tiles, and is a lot when the player uses a really big field
        //Find the nearest tile to this position
        float nearest = 999;
        Tile nearestTile = null;

        foreach (Tile tile in tiles)
        {
            float distance = Vector3.Distance(tile.GetPosition(), pos);
            if (distance < nearest)
            {
                nearest = distance;
                nearestTile = tile;
                if (nearest == 0) break; //Stops the loop early if we have the absolute nearest
            }
        }
        return nearestTile;
        */
        //We use the flipped formula which was used for calculating the initial coordinates instead
        int x = (int)Math.Round((((pos.x + 30f) * tilesInARow) - 30f) / 60f);
        int z = (int)Math.Round((((pos.z + 30f) * tilesInARow) - 30f) / 60f);
        return tiles[x * tilesInARow + z];
    }

    protected void OnTileNotify(object sender, Tile.OnNotifyFromTileArgs a)
    {
        if (a.Message.Equals("Reveal"))
        {
            IncreaseRevealedTiles();
            if (openTiles == totalTiles - totalBombs)
            {
                EndGame("YOU WIN!");
            }
        }
        else if (a.Message.Equals("Bomb"))
        {
            EndGame("YOU LOSE!");
        }
        else if (a.Message.Equals("Mark"))
        {
            IncreaseMarkedTiles();
        }
        else if (a.Message.Equals("Unmark"))
        {
            DecreaseMarkedTiles();
        }
    }

    private void EndGame(string s)
    {
        //Save data
        PlayerPrefs.SetString("Time", "TIME: " + Math.Round(Time.timeSinceLevelLoad, 2).ToString() + " SECONDS");
        PlayerPrefs.SetString("Revealed", "TILES OPENED: " + Math.Round(((float)openTiles / (totalTiles - totalBombs)) * 100.0f, 2) + "%");
        PlayerPrefs.SetString("Difficulty", "TILES: " + totalTiles + ", BOMBS: " + totalBombs);

        PlayerPrefs.SetString("Result", s);
        SceneManager.LoadScene("GameOverScene");
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }
}
